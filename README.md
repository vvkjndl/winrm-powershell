# WinRM PowerShell

## winrm-powershell.ps1

This script will help you in managing WinRM on server-side.

- Only AllowRemoteShellAccess is enabled on server-side. Any other authentication mechanisms are kept disabled.
- Only HTTPS/SSL connections will be accepted.
- Use this [**repo**](https://gitlab.com/vvkjndl/ngrok-winacme "Ngrok WinACME") to generate LetsEncrypt CA certificates for WinRM server.

### Usage

- IIS must be installed and Default Web Site must be configured with correct public hostname.
- A valid certificate must be present in personal store with correct public hostname (FQDN).
- Clone this repository on desktop and import the script as module.

`
Import-Module -FullyQualifiedName "$home\Desktop\winrm-powershell\winrm-powershell.ps1" -DisableNameChecking -Force
`

- You can then use below commands to manage WinRM state on server-side.

```
WinRM-ServerState       Manages server-side WinRM to allow remote commands. (RunAsAdmin)
  -Enable               Enables WinRM service.
  -Disable              Disables WinRM service.

WinRM-Help              Shows this help page.
```
