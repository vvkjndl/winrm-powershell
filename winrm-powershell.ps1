# this function will manage winrm server-side state
function global:WinRM-ServerState {

    param (

        [Parameter(Mandatory = $False)]
        [switch] $Disable,

        [Parameter(Mandatory = $False)]
        [switch] $Enable

    )

    if ($Disable) {
        Write-Host -Object "Disabling PSRemoting..."
        Disable-PSRemoting -Force 3>&1 | Out-Null
        Write-Host -Object "Removing WinRM listeners..."
        Start-Process -FilePath "cmd.exe" -ArgumentList '/C winrm.cmd delete winrm/config/Listener?Address=*+Transport=HTTP' -Wait -Verb RunAs
        Start-Process -FilePath "cmd.exe" -ArgumentList '/C winrm.cmd delete winrm/config/Listener?Address=*+Transport=HTTPS' -Wait -Verb RunAs
        Write-Host -Object "Disabling LocalAccountTokenFilterPolicy..."
        Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name "LocalAccountTokenFilterPolicy" -Value "0x0" -Type "DWORD"
        Write-Host -Object "Reverting firewall exceptions..."
        Get-NetFirewallRule | Where-Object {$_.Name -like "*winrm*"} | ForEach-Object {
            Remove-NetFirewallRule -InputObject $_
        }
        Write-Host -Object "Stopping WinRM service..."
        Stop-Service -Name "winrm"
        Write-Host -Object "Setting WinRM service AutoStartType to Manual..."
        Set-Service -Name "winrm" -StartupType "Manual"
    }

    if ($Enable) {
        # http 5985 firewall rule is needed for Enable-PSRemoting command to work
        $FirewallRule = @{
            Name        = 'WINRM-HTTP-In-TCP'
            DisplayName = 'Windows Remote Management (HTTP-In)'
            Description = 'Inbound rule for Windows Remote Management via WS-Management. [TCP 5985]'
            Direction   = 'Inbound'
            LocalPort   = 5985
            Protocol    = 'TCP'
            Action      = 'Allow'
            Program     = 'System'
        }
        New-NetFirewallRule @FirewallRule *> $null
        Write-Host -Object "Enabling PSRemoting..."
        Enable-PSRemoting -SkipNetworkProfileCheck -Force | Out-Null
        Set-WSManInstance -ResourceURI winrm/config/Winrs -ValueSet @{AllowRemoteShellAccess = "true"} | Out-Null
        Write-Host -Object "Disabling WSMan tracing capabilities..."
        Disable-WSManTrace | Out-Null
        Write-Host -Object "Disabling CredSSP authentication..."
        Disable-WSManCredSSP -Role "Client" | Out-Null
        Disable-WSManCredSSP -Role "Server" | Out-Null
        Write-Host -Object "Disabling unwanted authentication mechanisms on service..."
        Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WSMAN\Service" -Name "auth_basic" -Value "0x0" -Type "DWORD"
        Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WSMAN\Service" -Name "auth_credssp" -Value "0x0" -Type "DWORD"
        Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WSMAN\Service" -Name "auth_kerberos" -Value "0x0" -Type "DWORD"
        Write-Host -Object "Setting CbtHardeningLevel to strict..."
        Set-WSManInstance -ResourceURI winrm/config/service/auth -ValueSet @{CbtHardeningLevel = "Strict"} | Out-Null
        Write-Host -Object "Disabling unwanted authentication mechanisms on client..."
        Set-WSManInstance -ResourceURI winrm/config/client/auth -ValueSet @{Basic = "false"; Digest = "false"; Kerberos = "false"; CredSSP = "false"; Certificate = "false"} | Out-Null
        Set-WSManInstance -ResourceURI winrm/config/client -ValueSet @{AllowUnencrypted = "false"} | Out-Null
        # this value is in milliseconds; 120000 milliseconds = 2 minutes
        Set-WSManInstance -ResourceURI winrm/config/Winrs -ValueSet @{IdleTimeout = "120000"} | Out-Null
        Start-Process -FilePath "cmd.exe" -ArgumentList '/C winrm.cmd delete winrm/config/Listener?Address=*+Transport=HTTP' -Wait -Verb RunAs
        Start-Process -FilePath "cmd.exe" -ArgumentList '/C winrm.cmd delete winrm/config/Listener?Address=*+Transport=HTTPS' -Wait -Verb RunAs
        Get-NetFirewallRule | Where-Object {$_.Name -like "*winrm*"} | ForEach-Object {
            Remove-NetFirewallRule -InputObject $_
        }
        # determine hostname
        $iishostname = (Get-IISSiteBinding -Name "Default Web Site" -Protocol http).bindingInformation.Split(':')[-1]
        # obtain certificate thumbprint
        $certificatethumbprint = (Get-ChildItem -Path Cert:LocalMachine\My | Where-Object {$_.Subject -eq "CN=$iishostname"}).Thumbprint
        # create winrm https listener
        Write-Host -Object "Creating HTTPS WinRM listener using hostname as $iishostname and certificate thumbprint as $certificatethumbprint..."
        $processargs = "/C winrm.cmd create winrm/config/Listener?Address=*+Transport=HTTPS `"@{Hostname = `"$iishostname`"; CertificateThumbprint = `"$certificatethumbprint`"}`""
        Start-Process -FilePath "cmd.exe" -ArgumentList $processargs -Wait -Verb RunAs
        # create firewall rule for winrm https
        Write-Host -Object "Creating firewall inbound rule for WinRM over HTTPS."
        $FirewallRule = @{
            Name        = 'WINRM-HTTPS-In-TCP'
            DisplayName = 'Windows Remote Management (HTTPS-In)'
            Description = 'Inbound rule for Windows Remote Management via WS-Management. [TCP 5986]'
            Direction   = 'Inbound'
            LocalPort   = 5986
            Protocol    = 'TCP'
            Action      = 'Allow'
            Program     = 'System'
        }
        New-NetFirewallRule @FirewallRule *> $null
        Get-NetFirewallRule | Where-Object {$_.Name -like "*winrm*https*"} | Set-NetFirewallRule -Enabled "True"
        Write-Host -Object "Restarting WinRM service..."
        Restart-Service -Name "winrm" -Force | Out-Null
    }

    if (!($Enable) -and !($Disable)) {
        Write-Warning -Message "You must specify either -Enable or -Disable."
    }

}


# this function will display usage help on console
function global:WinRM-Help {

    Write-Host -NoNewline -Object "`n"

    Write-Host -NoNewline -ForegroundColor Yellow -Object "WinRM-ServerState`t"
    Write-Host -NoNewline -Object "Manages server-side WinRM to allow remote commands."
    Write-Host -ForegroundColor Yellow -Object " (RunAsAdmin)"
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -Enable`t`t"
    Write-Host -Object "Enables WinRM service."
    Write-Host -NoNewline -ForegroundColor Yellow -Object "  -Disable`t`t"
    Write-Host -Object "Disables WinRM service."

    Write-Host -NoNewline -Object "`n"
    Write-Host -NoNewline -ForegroundColor Yellow -Object "WinRM-Help`t`t"
    Write-Host -Object "Shows this help page."

    Write-Host -NoNewline -Object "`n"

}


# show help on first run
WinRM-Help